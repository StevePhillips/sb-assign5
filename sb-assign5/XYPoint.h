// ------------------------------
// XYPoint.h, Steve Bennett
// --------------------------
// Written out manually by myself
// ------------------------------
#import <Foundation/Foundation.h>
// ------------------------------
@interface XYPoint : NSObject
// --------------------------
@property int x, y;
// ----------------
- (void) setX:(int)xval andY:(int)yval;
@end
