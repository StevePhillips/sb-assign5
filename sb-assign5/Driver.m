// -------------------------------
// Driver.m, Steve Bennett
// -------------------------
// Implements the driver class. The driver class is to demonstrait all
// the functionality implemented in the rectangle class
// ----------------------------
#import <Foundation/Foundation.h>
// ------------------------
#import "Driver.h"
#import "Rectangle.h"
#import "XYPoint.h"
// -----------------
@implementation Driver

-(void) run
{
  Rectangle* rectangleA = [[Rectangle alloc] init];
  Rectangle* rectangleB = [[Rectangle alloc] init];
  // ----------------------------------------
  XYPoint* pointA = [[XYPoint alloc] init];
  XYPoint* pointB = [[XYPoint alloc] init];
  // --------------------------------------
  [pointA setX:0 andY:0];
  [pointB setX: 5 andY:4];
  // --------------------
  rectangleA.origin = pointA;
  rectangleB.origin = pointB;
  // ------------------------
  [rectangleA setWidth:10 andHeight:8];
  [rectangleB setWidth:5 andHeight:6];
  // -------------------------------------------------------------
  // Take note that rectangles draw 2 dashes per 1 point of width
  // ------------------------------------
  puts("Rectangle A (0,0) with (10,8):");
  [rectangleA draw];
  puts("Rectangle B (5,4) with (3,6):");
  [rectangleB draw];
  // ---------------
  if([rectangleA containsPoint:pointB])
    puts("rectangleA contains the point (5,4)");
  // -------------------------------------------
  Rectangle* rect = [rectangleA intersect: rectangleB];
  // --------------------------------------------------
  if(rect.width > 0)
  {
    puts("rectA and B intersect to make the rectangle:");
    [rect draw];
  }
  [rectangleB.origin setX:11 andY:8];
  // ---------------------
  rect = [rectangleA intersect: rectangleB];
  // -----------------------
  if(rect.width == 0)
    puts("There is now no intersect");
}
@end