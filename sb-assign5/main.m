// ------------------------------------------
// main.m, by Steve Bennett
// sb-assign5
// ---------------------
// The actual driver of this program
// It calls the driver class run method
// -------------------------------------
#import <Foundation/Foundation.h>
#import "Driver.h"
// ---------------
int main(int argc, const char * argv[])
{
  @autoreleasepool
  {
    [[[Driver alloc] init] run];
  }
  return 0;
}
