// ------------------------------
// Rectangle.m, Steve Bennett
// --------------------------
#import "XYPoint.h"
#import "Rectangle.h"
@implementation Rectangle
{
  XYPoint * origin;
}
@synthesize width, height;
// ----------------------
- (void) setWidth:(int)w andHeight:(int)h
{
  width = w;
  height = h;
}
- (void) setOrigin:(XYPoint *)pt
{
  origin = pt;
}
- (int) area
{
  return width * height;
}
- (int) perimeter
{
  return (width + height) * 2;
}
- (XYPoint *) origin
{
  return origin;
}
- (BOOL) containsPoint:(XYPoint *)aPoint
{
  if ((origin.x < aPoint.x && aPoint.x < origin.x + width) &&
      (origin.y < aPoint.y && aPoint.y < origin.y + height))
  {
    return YES;
  }
  return NO;
}
- (void) draw
{
  // Draw the top of the rectangle
  for (int iter = 0; iter < width; iter++)
  {
    // fputs does not output end of line
    fputs("--", stdout);
  }
  putchar('\n');
  // Draw the vertical part of the rectangle
  // for each bar draw the left bar, the appropriate
  // ammount of spaces, then the right bar
  for (int iter = 0; iter < height; iter++)
  {
    putchar('|');
    for (int iter2 = 0; iter2 < width - 2; iter2++)
    {
      //putchar(' ');
      fputs("  ", stdout);
    }
    // puts does output end of line
    puts("  |");
  }
  for (int iter = 0; iter < width; iter++)
  {
    fputs("--", stdout);
  }
  putchar('\n');
}
- (Rectangle *) intersect:(Rectangle *)other
{
  // 1. figure out which rectangle is on the *right*
  // 2. does the left rectangle contain 'intersect' the right's origin
  // 3. if so, return a rectangle with the origin of the right but whos
  // with and height are (left.width, left.height) - (right.org.x, right.org.y)
  Rectangle * right = 0;
  Rectangle * left = 0;
  // --------------------------------------------------------------
  if (origin.x > [other origin].x)
  {
    right = self;
    left = other;
  }
  else
  {
    right = other;
    left = self;
  }
  Rectangle * retRect = [[Rectangle alloc] init];
  // -----------------------------------
  if ([left containsPoint:right.origin])
  {
    [retRect setOrigin:right.origin];
    [retRect setWidth:(left.origin.x + left.width) - right.origin.x andHeight
                     :left.origin.y + left.height - right.origin.y];
    return retRect;
  }
  // --------------------------------------
  XYPoint * xypoint = [[XYPoint alloc] init];
  // -----------------------
  [xypoint setX:0 andY:0];
  [retRect setOrigin:xypoint];
  [retRect setWidth:0 andHeight:0];
  return retRect;
}
@end
