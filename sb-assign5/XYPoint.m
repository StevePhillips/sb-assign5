// --------------------------
// XYPoint.h by Steve Bennett
// --------------------------
// Written manually by myself
// --------------------------
#import "XYPoint.h"
// ----------------
@implementation XYPoint
// --------------------
@synthesize x, y;
// --------------
- (void) setX:(int)xval andY:(int)yval
{
  x = xval;
  y = yval;
}
@end
