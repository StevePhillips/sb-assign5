// -------------------------
// Driver.h, Steve Bennett
// -----------------------
// Fully demonstraits the rectangle class by having its run method called
// -----------------------------------------------------------------------
@interface Driver : NSObject
// -------------------------
-(void) run;
// ---------
@end
