// ------------------------------
// Rectangle.h, Steve Bennett
// --------------------------------------------------------
// Defines and implements draw, containsPoint and intersect
// as per requirements of the assignment and the book
// --------------------------------------------------
#import <Foundation/Foundation.h>
// --------------------------
@class XYPoint;
// ---------------
@interface Rectangle : NSObject
// --------------
@property int width, height;
// -------------------------
- (XYPoint *) origin;
// -----------------
- (void) setOrigin:(XYPoint *)pt;
// Note that if the colon precedes the arg type
// the compiler wont complain about what its refering to.
- (void) setWidth:(int)w andHeight:(int)h;
// -----------------------------------------
- (int) area;
// ---------
- (int) perimeter;
// --------------
- (void) draw;
// ----------
- (BOOL) containsPoint:(XYPoint *)aPoint;
// -------------------------------------
- (Rectangle *) intersect:(Rectangle *)other;
@end
